///////////////////////////////////////////////////////////////////////////////
//                             Lisaac Compiler                               //
//                                                                           //
//                   LSIIT - ULP - CNRS - INRIA - FRANCE                     //
//                                                                           //
//   This program is free software: you can redistribute it and/or modify    //
//   it under the terms of the GNU General Public License as published by    //
//   the Free Software Foundation, either version 3 of the License, or       //
//   (at your option) any later version.                                     //
//                                                                           //
//   This program is distributed in the hope that it will be useful,         //
//   but WITHOUT ANY WARRANTY; without even the implied warranty of          //
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           //
//   GNU General Public License for more details.                            //
//                                                                           //
//   You should have received a copy of the GNU General Public License       //
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.   //
//                                                                           //
//                     http://isaacproject.u-strasbg.fr/                     //
///////////////////////////////////////////////////////////////////////////////
Section Header
  
  + name        := ITM_TYPE_MULTI;

  - copyright   := "2003-2007 Benoit Sonntag";

  
  - author      := "Sonntag Benoit (bsonntag@loria.fr)";
  - comment     := "List of type";
  
Section Inherit
  
  + parent_itm_type:Expanded ITM_TYPE;
  
Section Private
  
  - dico:FAST_ARRAY[ITM_TYPE_MULTI] := FAST_ARRAY[ITM_TYPE_MULTI].create_with_capacity 32;

  - create lt:FAST_ARRAY[ITM_TYPE_MONO] :SELF <-
  ( + result:SELF;
    
    result := clone;
    result.make lt;
    result
  );
  
  - make lt:FAST_ARRAY[ITM_TYPE_MONO] <-
  (
    list_type := lt;
  );
 
Section Public
  
  + list_type:FAST_ARRAY[ITM_TYPE_MONO];
  
  - count:INTEGER <- list_type.count;
  
  - upper:INTEGER <- list_type.upper;
  
  - item i:INTEGER :ITM_TYPE_MONO <-
  (
    list_type.item i
  );
  
  - last:ITM_TYPE_MONO <-
  (
    list_type.last
  );

  - first:ITM_TYPE_MONO <-
  (
    list_type.first
  );
  
  - get lt:FAST_ARRAY[ITM_TYPE_MONO] :ITM_TYPE_MULTI <-
  ( + result:ITM_TYPE_MULTI;
    + idx:INTEGER;
    
    idx := dico.lower;
    {(idx <= dico.upper) && {dico.item idx.list_type != lt}}.while_do {
      idx := idx + 1;
    };    
    (idx <= dico.upper).if {
      result := dico.item idx;
    } else {
      result := create lt;
      dico.add_last result;
    };
    result
  );
  
  - display buffer:STRING <-
  (    
    buffer.add_last '(';
    (list_type.lower).to (list_type.upper - 1) do { j:INTEGER;
      list_type.item j.display buffer;
      buffer.add_last ',';
    };
    list_type.last.display buffer;
    buffer.add_last ')';
  );
