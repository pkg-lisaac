///////////////////////////////////////////////////////////////////////////////
//                             Lisaac Compiler                               //
//                                                                           //
//                   LSIIT - ULP - CNRS - INRIA - FRANCE                     //
//                                                                           //
//   This program is free software: you can redistribute it and/or modify    //
//   it under the terms of the GNU General Public License as published by    //
//   the Free Software Foundation, either version 3 of the License, or       //
//   (at your option) any later version.                                     //
//                                                                           //
//   This program is distributed in the hope that it will be useful,         //
//   but WITHOUT ANY WARRANTY; without even the implied warranty of          //
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           //
//   GNU General Public License for more details.                            //
//                                                                           //
//   You should have received a copy of the GNU General Public License       //
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.   //
//                                                                           //
//                     http://isaacproject.u-strasbg.fr/                     //
///////////////////////////////////////////////////////////////////////////////
Section Header
  
  + name        := ITM_TYPE_STYLE;

  - copyright   := "2003-2007 Benoit Sonntag";

  
  - author      := "Sonntag Benoit (bsonntag@loria.fr)";
  - comment     := "Type with style";
  
Section Inherit
  
  + parent_itm_type_simple:Expanded ITM_TYPE_SIMPLE;
  
Section Private 
  
  - dico:FAST_ARRAY[ITM_TYPE_STYLE] := FAST_ARRAY[ITM_TYPE_STYLE].create_with_capacity 32;
  
  - create n:STRING_CONSTANT style s:STRING_CONSTANT :SELF <-
  ( + result:SELF;
    
    result := clone;
    result.make n style s;
    result
  );
  
  - make n:STRING_CONSTANT style s:STRING_CONSTANT <-
  (
    name  := n;
    style := s;
  );
  
Section Public
    
  - hash_code:INTEGER <- name.hash_code;
  
  + style:STRING_CONSTANT;
  
  - get n:STRING_CONSTANT style s:STRING_CONSTANT :ITM_TYPE_STYLE <-
  ( + result:ITM_TYPE_STYLE;
    + idx:INTEGER;
    
    idx := dico.lower;
    {
      (idx <= dico.upper) && {
	(dico.item idx.name  != n) || 
	{dico.item idx.style != s}
      }
    }.while_do {
      idx := idx + 1;
    };
    (idx <= dico.upper).if {
      result := dico.item idx;
    } else {
      result := create n style s;
      dico.add_last result;
    };
    result
  );

  + to_run:TYPE_FULL <-
  (
    to_run := TYPE.get Self 
  );

  - display buffer:STRING <-
  (
    buffer.append style;
    buffer.add_last ' ';
    buffer.append name;
  );

