///////////////////////////////////////////////////////////////////////////////
//                             Lisaac Library                                //
//                                                                           //
//                   LSIIT - ULP - CNRS - INRIA - FRANCE                     //
//                                                                           //
//   This program is free software: you can redistribute it and/or modify    //
//   it under the terms of the GNU General Public License as published by    //
//   the Free Software Foundation, either version 3 of the License, or       //
//   (at your option) any later version.                                     //
//                                                                           //
//   This program is distributed in the hope that it will be useful,         //
//   but WITHOUT ANY WARRANTY; without even the implied warranty of          //
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           //
//   GNU General Public License for more details.                            //
//                                                                           //
//   You should have received a copy of the GNU General Public License       //
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.   //
//                                                                           //
//                     http://isaacproject.u-strasbg.fr/                     //
///////////////////////////////////////////////////////////////////////////////
Section Header

  + name    := G_WIN_OUT;


  - copyright   := "2003-2005 Jérome Boutet, 2003-2007 Benoit Sonntag";
      
  - author  := "Sonntag Benoit (bsonntag@loria.fr)";
  - comment := "Group elements for GUI.";

Section Inherit  
  
  + parent_g_group:Expanded G_GROUP;
  
Section Public  
  
  + title_len:INTEGER;
  
  + title:ABSTRACT_STRING;
  
  + stat:INTEGER_8;
    
  - is_open:BOOLEAN <- (stat & 1) != 0;
  
  - open_by src:G_GROUP <-
  ( + px,py,w,h:INTEGER;
    
    (stat = 0).if {	
      ((title = NULL) && {src != NULL}).if {	
	w := width_min;
	h := height_min;
	(src.is_horizontal).if {
	  px := src.x_window;
	  py := src.y_window + src.height;
	  ((px + w) > INTERFACE.x_max).if {
	    px := INTERFACE.x_max - w;
	  };
	  ((py + h) > INTERFACE.y_max).if {
	    py := src.y_window - h;
	  };
	} else {
	  px := src.x_window + src.width;
	  py := src.y_window;
	  ((px + w) > INTERFACE.x_max).if {
	    px := src.x_window - w;
	  };
	  ((py + h) > INTERFACE.y_max).if {
	    py := INTERFACE.y_max - h;
	  };
	};	  	  
      } else {
	px := (INTERFACE.width  - width_min ) / 2;
	py := (INTERFACE.height - height_min) / 2;
      };
      set_position INTERFACE at (px,py);
      stat := 01b;	
    };       
  );
    
  - close <-
  (
    delete;
    stat := 0;
  );
  
  //
  // Creation.
  //
  
  - create t:ABSTRACT_STRING with elt:G_EXPR :SELF <-
  ( + result:SELF;
    
    result := clone;
    result.make t with elt;
    result
  );
  
  - make t:ABSTRACT_STRING with elt:G_EXPR <-
  (
    title_len := BITMAP[PIXEL_32].font_width t + 2; //+ 25;
    title := t;
    root := elt;
  );
  
  //
  // Size.
  //
  
  - width_min:INTEGER <-
  (
    parent_g_group.width_min + 2
  );
  
  - height_min:INTEGER <-
  ( + result:INTEGER;
    
    result := parent_g_group.height_min;
    (title = NULL).if {
      result := result + 2;
    } else {
      result := result + 17 + 1
    };
    result
  );

  //
  // Update position.
  //
    
  - set_position rac:AREA at (x,y:INTEGER) size (w,h:INTEGER) <-
  (
    update rac from (x,y) size (w,h);
    (title = NULL).if {
      root.set_position Self at (1, 1) size (w-2,h-2);
    } else {
      root.set_position Self at (1,17) size (w-2,h-18);
    };
  );
  
  //
  // Display.
  //
  
  - draw_slave bmp:ABSTRACT_BITMAP from (x0,y0:INTEGER) to (x1,y1:INTEGER) <-
  (
    bmp.rectangle_fill (x0,y0) to (x1,y1) color color_back; 
  );
  
  - draw (x0,y0:INTEGER) to (x1,y1:INTEGER) <-
  ( 
    clipping (x0,y0) to (x1,y1);    
    
    (title = NULL).if {      
      rectangle (0,0) to (x_max,y_max) color black;
      draw_slave Self from (1,1) to (x_max-1,y_max-1);      
    } else {
      color 646496h;
      line_v (0,15) until 0;
      line_h_until (title_len+1);
      line_to ((title_len+1+15),15);
        
      // Border out.
      rectangle (0,16) to (x_max,y_max) color black;
      draw_slave Self from (1,17) to (x_max-1,y_max-1);
            
      // Title background.
      color 0C8C8FFh;
      poly_move_to (1,1);
      poly_line_to ((title_len+1),1);
      poly_line_to ((title_len+15),15);
      poly_line_to (1,15);
      poly_trace;
    
      // Title.
      ((stat & 10b) = 0).if {
	color black;
      } else {
	color red;
      };    
      print title to (3,(-1));    
    };
  );
  
  - slave_pixel_hard (x,y:INTEGER) color col:UINTEGER_32 <- 
  ( 
    (title != NULL).if {
      ( ((y<16) && {x>(title_len+1+y)}) || 
      { (y.in_range 20 to (y_max-4)) && {x.in_range 4 to (x_max-4)}} ).if {
	pixel_hard (x,y) color col;
      };
    };
  );
  
  - slave_line_h_hard (x1,y:INTEGER) until x2:INTEGER color col:UINTEGER_32 <- 
  ( + new_x1,new_x2:INTEGER;
    
    (title != NULL).if {
      ((y<16) && {x2>(title_len+1+y)}).if {
	new_x1:=x1.max (title_len+2+y);
	line_h_hard (new_x1,y) until x2 color col;
      }.elseif {(y.in_range 20 to (y_max-4)) && {x1<(x_max-3)} && {x2>3}} then {
	new_x1:=x1.max 4;
	new_x2:=x2.min (x_max-4);
	line_h_hard (new_x1,y) until new_x2 color col;
      };
    };
  );

  - slave_line_h_hard (x1,y:INTEGER) until x2:INTEGER 
  image line:ABSTRACT_BMP_LINE offset ofs:INTEGER <-
  ( + new_x1,new_x2,new_ofs:INTEGER;
    
    (title != NULL).if {
      ((y<16) && {x2>(title_len+1+y)}).if {
	new_x1  := x1.max (title_len+2+y);
	new_ofs := ofs + new_x1 - x1;
	line_h_hard (new_x1,y) until x2 image line offset new_ofs;
      }.elseif {(y.in_range 20 to (y_max-4)) && {x1<(x_max-3)} && {x2>3}} then {
	new_x1  := x1.max 4;
	new_x2  := x2.min (x_max-4);
	new_ofs := ofs + new_x1 - x1;
	line_h_hard (new_x1,y) until new_x2 image line offset new_ofs;
      };
    };
  );

  //
  // Event.
  //
    
  - receive msg:EVENT <-  
  ( + mouse:EVENT_MOUSE;
    + win:AREA;    

    mouse ?= msg;        
    (mouse != NULL).if {       
      (mouse.right_down).if {
	first;
      };
      ((mouse.is_moving_only) && {mouse.left}).if {
	move ((mouse.dx),(mouse.dy));
      } else {	
	win := INTERFACE.get_object ((mouse.x_current),(mouse.y_current));
	(win != Self).if {	  
	  INTERFACE.receive msg;
	};
      };
    };
  );
    
  - get_object (x,y:INTEGER) :AREA <-
  ( + result:AREA;
    + rel_x,rel_y:INTEGER;
    
    result := parent_g_group.get_object (x,y);
    
    (result = Self).if {
      (title != NULL).if {
	rel_x := x - x_window;
	rel_y := y - y_window;
	((rel_y >= 16) || {rel_x > (title_len+1+rel_y)}).if {
	  result := NULL;
	};
      };
    };
    result
  );
/*    
  - receive msg:EVENT <-  
  ( + evt_mouse:EVENT_MOUSE;
    + win:AREA;
    
    evt_mouse ?= msg;
    (evt_mouse != NULL).if { 
      (evt_mouse.right_down).if {
	first;
      };
      ((evt_mouse.is_moving_only) && {evt_mouse.left}).if {
	move ((evt_mouse.dx),(evt_mouse.dy));
      } else {
	win := INTERFACE.get_object ((evt_mouse.x_current),(evt_mouse.y_current));
	(win != Self).if {
	  stat := 0;
	  refresh;
	  INTERFACE.receive msg;
	} else {
	  (stat != 1).if {
	    stat := 1;
	    refresh;
	  };
	};
      };
    };
  );
*/  
  //
  // Message.
  //
/*  
  - close <-
  ( 
    (is_reduce).if {
      INTERFACE.screen.remove_bar self;
    };
    delete;
  );
  
  + x_old:INTEGER;
  
  + y_old:INTEGER;
  
  - reduce <-
  ( 
    (is_reduce).if {
      INTERFACE.screen.remove_bar self;  
      set_position x_old,y_old;
    } else {
      x_old := get_x_window;
      y_old := get_y_window;
      INTERFACE.screen.add_bar self;      
    };    
    is_reduce := ! is_reduce;
  );

  - action who:OBJECT <-
  (
  );
  */