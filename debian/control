Source: lisaac
Section: devel
Priority: optional
Maintainer: picca frederic <picca@synchrotron-soleil.fr>
Uploaders: Xavier Oswald <xoswald@debian.org>, Aurélien GÉRÔME <ag@debian.org>
Build-Depends: debhelper (>= 7), gcc (>= 4.1), emacs23 | emacsen
Standards-Version: 3.8.4
Homepage: http://isaacproject.u-strasbg.fr/
DM-Upload-Allowed: yes

Package: lisaac
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, lisaac-common, gcc(>= 4.1)
Recommends: lisaac-doc
Suggests: lisaac-mode
Description: Object-oriented language base on prototype
 Lisaac is a small prototype-based programming language. The ideas in
 Lisaac are mostly inspired by Smalltalk (all values are objects), Self
 (prototype-based) and Eiffel (design by contract). It has the following
 features:
 .
  * pure object language
  * very fast (like C code)
  * dynamic and multiple inheritance
  * dynamic definition slots
  * static typing (invariant)
  * generic types
  * auto-cast type system
  * programming by contract
  * interrupt manager
  * include C code facilities

Package: lisaac-common
Architecture: all
Depends: ${misc:Depends}
Description: Arch-independent part for lisaac
 Lisaac is a small prototype-based programming language.
 .
 This package contains the libraries needed by the Lisaac compiler.

Package: lisaac-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: Documentation for lisaac
 Lisaac is a small prototype-based programming language.
 .
 This package contains the documentation in html format.

Package: lisaac-mode
Architecture: all
Depends: ${misc:Depends}, emacs23 | emacsen
Enhances: lisaac
Description: Emacs mode for editing Lisaac programs
 Lisaac is a small prototype-based programming language.
 .
 This emacs mode provides syntax highlighting and automatic
 indentation for Lisaac. you will need this if you write Lisaac programs
 using Emacs.
